package com.example.erlangga.mymovieapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.module.AppGlideModule;
import com.bumptech.glide.request.target.Target;
import com.example.erlangga.mymovieapp.BuildConfig;
import com.example.erlangga.mymovieapp.R;
import com.example.erlangga.mymovieapp.activities.MainActivity;
import com.example.erlangga.mymovieapp.activities.MovieDetailsActivity;
import com.example.erlangga.mymovieapp.helpers.GlideApp;
import com.example.erlangga.mymovieapp.helpers.MyAppGlideModule;
import com.example.erlangga.mymovieapp.models.MovieItems;

import java.util.ArrayList;

public class RecyclerViewMovieItemAdapter extends RecyclerView.Adapter<RecyclerViewMovieItemAdapter.MovieItemViewHolder> {
    private ArrayList<MovieItems> mData = new ArrayList<>();
    private Context mContext;

    public RecyclerViewMovieItemAdapter(Context context) {
        this.mContext = context;
    }

    public void setData(ArrayList<MovieItems> items) {
        Log.d("RecyclerAdapter", "set mData");
        mData = items;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MovieItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.movie_items, parent, false);
        MovieItemViewHolder holder = new MovieItemViewHolder(convertView);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MovieItemViewHolder viewHolder, final int position) {
        Log.d("RecyclerAdapter", "onBindViewHolder");
        String basicPosterUrl = BuildConfig.IMAGE_URL;
        viewHolder.textViewTitle.setText(mData.get(position).getTitle());
        viewHolder.textViewDescription.setText(mData.get(position).getDescription());
        GlideApp.with(viewHolder.convertView)
                .load(basicPosterUrl + mData.get(position).getPosterUrl())
                .override(Target.SIZE_ORIGINAL)
                .into(viewHolder.imagePoster);

        viewHolder.convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String item = mData.get(position).getTitle();
                MovieItems movieItems = mData.get(position);
                Intent movieDetailsObjectIntent = new Intent(mContext, MovieDetailsActivity.class);
                movieDetailsObjectIntent.putExtra(MovieDetailsActivity.EXTRA_MOVIE_DETAILS, movieItems);
                movieDetailsObjectIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                mContext.startActivity(movieDetailsObjectIntent);
                Log.d(MainActivity.class.getSimpleName(), item);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    static class MovieItemViewHolder extends RecyclerView.ViewHolder {
        TextView textViewTitle;
        TextView textViewDescription;
        ImageView imagePoster;
        View convertView;

        public MovieItemViewHolder(@NonNull View convertView) {
            super(convertView);
            this.convertView = convertView;
            this.textViewTitle = convertView.findViewById(R.id.tv_title);
            this.textViewDescription = convertView.findViewById(R.id.tv_description);
            this.imagePoster = convertView.findViewById(R.id.img_poster);
        }
    }
}
