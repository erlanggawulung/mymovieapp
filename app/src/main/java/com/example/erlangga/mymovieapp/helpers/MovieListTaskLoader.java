package com.example.erlangga.mymovieapp.helpers;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import com.example.erlangga.mymovieapp.BuildConfig;
import com.example.erlangga.mymovieapp.models.MovieItems;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.SyncHttpClient;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class MovieListTaskLoader extends AsyncTaskLoader<ArrayList<MovieItems>> {
    private static final String API_KEY = BuildConfig.TMDB_API_KEY;
    private ArrayList<MovieItems> mData;
    private boolean mHasResult = false;
    private String movieQuery;

    public MovieListTaskLoader(final Context context, String movieQuery) {
        super(context);

        onContentChanged();
        this.movieQuery = movieQuery;
    }

    @Override
    protected void onStartLoading() {
        if (takeContentChanged()) {
            forceLoad();
        } else if (mHasResult) {
            deliverResult(mData);
        }
    }

    @Override
    public void deliverResult(final ArrayList<MovieItems> data) {
        mData = data;
        mHasResult = true;
        super.deliverResult(data);
    }

    @Override
    protected void onReset() {
        super.onReset();
        onStopLoading();
        if (mHasResult) {
            onReleaseResources(mData);
            mData = null;
            mHasResult = false;
        }
    }

    @Nullable
    @Override
    public ArrayList<MovieItems> loadInBackground() {
        SyncHttpClient client = new SyncHttpClient();

        final ArrayList<MovieItems> movieItems = new ArrayList<>();
        String url = BuildConfig.BASE_URL + "search/movie?api_key=" + API_KEY + "&language=en-US&query=" + movieQuery;

        client.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                setUseSynchronousMode(true);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    String result = new String(responseBody);
                    JSONObject responseObject = new JSONObject(result);
                    JSONArray results = responseObject.getJSONArray("results");

                    for (int i = 0; i < results.length(); i++) {
                        JSONObject movie = results.getJSONObject(i);
                        MovieItems movieItem = new MovieItems(movie);
                        movieItems.add(movieItem);
                    }
                    Log.d(MovieListTaskLoader.class.getSimpleName(), result);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.d(MovieListTaskLoader.class.getSimpleName(), "onFailure");
            }
        });
        return movieItems;
    }

    protected void onReleaseResources(ArrayList<MovieItems> data) {
        // do nothing
    }
}
