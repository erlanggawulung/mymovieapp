package com.example.erlangga.mymovieapp.activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.example.erlangga.mymovieapp.R;
import com.example.erlangga.mymovieapp.adapters.RecyclerViewMovieItemAdapter;
import com.example.erlangga.mymovieapp.helpers.MovieListTaskLoader;
import com.example.erlangga.mymovieapp.models.MovieItems;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<ArrayList<MovieItems>> {
    static final String EXTRAS_MOVIE_TITLE = "EXTRAS_MOVIE_TITLE";
    @BindView(R.id.recycler_view_movie)
    RecyclerView recyclerView;
    @BindView(R.id.edt_movie)
    EditText editMovieQuery;
    @BindView(R.id.btn_search)
    Button btnSearch;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    RecyclerViewMovieItemAdapter recyclerAdapter;

    View.OnClickListener myListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            progressBar.setVisibility(View.VISIBLE);
            String movieQuery = editMovieQuery.getText().toString();

            if (TextUtils.isEmpty(movieQuery)) {
                progressBar.setVisibility(View.GONE);
                return;
            }

            Bundle bundle = new Bundle();
            bundle.putString(EXTRAS_MOVIE_TITLE, movieQuery);
            getSupportLoaderManager().restartLoader(0, bundle, MainActivity.this);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerAdapter = new RecyclerViewMovieItemAdapter(this);
        recyclerView.setAdapter(recyclerAdapter);
        recyclerView.setHasFixedSize(true);

        btnSearch.setOnClickListener(myListener);

        String movieQuery = editMovieQuery.getText().toString();
        Bundle bundle = new Bundle();
        bundle.putString(EXTRAS_MOVIE_TITLE, movieQuery);

        progressBar.setVisibility(View.GONE);
        getSupportLoaderManager().initLoader(0, bundle, this);

        recyclerView.setClickable(true);
    }

    @NonNull
    @Override
    public Loader<ArrayList<MovieItems>> onCreateLoader(int i, @Nullable Bundle bundle) {
        String movieQuery = "";
        if (bundle != null) {
            movieQuery = bundle.getString(EXTRAS_MOVIE_TITLE);
        }
        return new MovieListTaskLoader(this, movieQuery);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<ArrayList<MovieItems>> loader, ArrayList<MovieItems> movieItems) {
        progressBar.setVisibility(View.GONE);
        recyclerAdapter.setData(movieItems);
    }

    @Override
    public void onLoaderReset(@NonNull Loader<ArrayList<MovieItems>> loader) {
        progressBar.setVisibility(View.GONE);
    }
}
