package com.example.erlangga.mymovieapp.models;

import org.json.JSONArray;
import org.json.JSONObject;

public class MovieDetails {
    private String title;
    private String posterUrl;
    private double rating;
    private int duration;
    private String tagline;
    private String language;
    private String release;
    private String description;
    private String genres;

    public MovieDetails(JSONObject object) {
        String basicPosterUrl = "https://image.tmdb.org/t/p/w342";
        try {
            String title = object.getString("title");
            String posterUrl = object.getString("poster_path");
            double rating = object.getDouble("vote_average");
            int duration = object.getInt("runtime");
            String tagline = object.getString("tagline");
            String language = object.getString("original_language");
            String release = object.getString("release_date");
            String description = object.getString("overview");
            JSONArray genres = object.getJSONArray("genres");

            this.title = title;
            this.posterUrl = basicPosterUrl + posterUrl;
            this.rating = rating;
            this.duration = duration;
            this.tagline = tagline;
            this.language = language;
            this.release = release;
            this.description = description;
            for (int i = 0; i < genres.length(); i++) {
                if (i == 0) {
                    this.genres = genres.getJSONObject(i).getString("name");
                } else {
                    this.genres += ", " + genres.getJSONObject(i).getString("name");
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPosterUrl() {
        return posterUrl;
    }

    public void setPosterUrl(String posterUrl) {
        this.posterUrl = posterUrl;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getTagline() {
        return tagline;
    }

    public void setTagline(String tagline) {
        this.tagline = tagline;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getRelease() {
        return release;
    }

    public void setRelease(String release) {
        this.release = release;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGenres() {
        return genres;
    }

    public void setGenres(String genres) {
        this.genres = genres;
    }
}
